/*
 * END Scene
 * Copyright © 2019+ Alvaro Fustero Blasco
 *
 * alvfus@gmail.com
 */

#include <memory>
#include <basics/Scene>
#include <basics/Texture_2D>
#include <basics/Raster_Font>

namespace ProjectAurora
{

    class Help_Scene : public basics::Scene
    {

        typedef std::shared_ptr< basics::Texture_2D > Texture;

    public:

        enum State
        {
            LOADING,
            RUNNING,
        };

        State          state;
        bool           suspended;

        unsigned       canvas_width;
        unsigned       canvas_height;

        Texture btn_Menu; ///imagen del botn menu
        Texture textoHelp; ///imagen de las instrucciones
        float          xHelp, yHelp; ///posiciones del puntero

    public:

        Help_Scene();

        basics::Size2u get_view_size () override ///Calcula el tamñai de la pantalla
        {
            return { canvas_width, canvas_height };
        }

        bool initialize () override;
        void suspend    () override;
        void resume     () override;

        void handle     (basics::Event & event) override;
        void update     (float time) override;
        void render     (basics::Graphics_Context::Accessor & context) override;

    private:

        void load ();
        void run  (float time);

        void ReturnMenu();
    };

}
