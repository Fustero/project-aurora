/*
 * END Scene
 * Copyright © 2019+ Alvaro Fustero Blasco
 *
 * alvfus@gmail.com
 */

#include "END_Scene.hpp"
#include "Play_Scene.hpp"
#include "Main_Menu.hpp"
#include <basics/Canvas>
#include <basics/Director>
#include <basics/Log>
#include <basics/Scaling>
#include <basics/Rotation>
#include <basics/Translation>

using namespace basics;
using namespace std;

namespace ProjectAurora
{

    /**
     * Asigna el tamaño del canvas a la escena
     */
    END_Scene::END_Scene()
    {
        canvas_width  = 1280;
        canvas_height =  720;
    }

    /**
     * Asigna el estado de cargando, de no suspendido y las pociciones iniciales del puntero
     */
    bool END_Scene::initialize ()
    {
        state     = LOADING;
        suspended = false;
        xHelp         = 640;
        yHelp         = 360;

        return true;
    }

    /**
     * Suspende la escena en 2º plano
     */
    void END_Scene::suspend ()
    {
        suspended = true;
    }

    /**
     * Se retoma la escena al volver.
     */
    void END_Scene::resume ()
    {
        suspended = false;
    }

    /**
     * Recoge la pulsación en pantalla y si se esta ejecutando la aplicacion recoge los valores y se los asigna al puntero.
     */
    void END_Scene::handle (Event & event)
    {
        if (state == RUNNING)
        {
            switch (event.id)
            {
                case ID(touch-started):
                case ID(touch-moved):
                case ID(touch-ended):
                {
                    xHelp = *event[ID(x)].as< var::Float > ();
                    yHelp = *event[ID(y)].as< var::Float > ();
                    break;
                }
            }
        }
    }

    /**
     * Dependiendo del estado se ejecuta una u otra.
     */
    void END_Scene::update (float time)
    {
        switch (state)
        {
            case LOADING: load ();     break;
            case RUNNING: run  (time); break;
        }
    }

    void END_Scene::render (basics::Graphics_Context::Accessor & context)
    {
        if (!suspended && state == RUNNING)
        {
            /**
             * Se crea el canvas si no existe
             */
            Canvas * canvas = context->get_renderer< Canvas > (ID(canvas));

            if (!canvas)
            {
                canvas = Canvas::create (ID(canvas), context, {{ canvas_width, canvas_height }});
            }

            if (canvas)
            {
                canvas->clear        ();
                canvas->set_color    (1, 1, 1);///Se asigna un color de fondo al canvas

                /**
                 * Se crea un boton de ir al menu
                 */
                if (btn_Menu)
                {
                    canvas->fill_rectangle ({ (canvas_width/4)*2 , (canvas_height/4) }, { 100, 100 }, btn_Menu.get ());
                }

                /**
                 * Se crea el texto de que has muerto
                 */
                if (font)
                {
                     //Se dibujan textos con diferentes puntos de anclaje a partir de una cadena simple:

                    Text_Layout sample_text(*font, L"Has muerto..."); //points

                    canvas->draw_text ({ canvas_width/2, canvas_height/2 }, sample_text, CENTER);

                }
            }
        }
    }

    void END_Scene::load ()
    {
        if (!suspended)
        {
            Graphics_Context::Accessor context = director.lock_graphics_context ();

            /**
             * De haber contexto crea el boton del menu
             */
            if (context)
            {
                btn_Menu = Texture_2D::create (ID(btnMenu), context, "menu.png");

                if (btn_Menu)
                {
                    context->add (btn_Menu);

                    state = RUNNING;
                }
            }
        }
    }

    void END_Scene::run (float )
    {
        /**
         * De no haber fueente la crea
         */
        if (!font)
        {
            Graphics_Context::Accessor context = director.lock_graphics_context ();

            if (context)
            {
                font.reset (new Raster_Font("fonts/impact.fnt", context));
            }
        }
        ReturnMenu();
    }

    void END_Scene::ReturnMenu()
    {

        /**
         * De estar el puntero sobre el boton del menu se pasa de escena
         */
        if ((xHelp < ((canvas_width/4)*2+50)) && (xHelp > ((canvas_width/4)*2-50)))
        {
            if (yHelp < (canvas_height/4)+50 && yHelp > (canvas_height/4)-50) //exit
            {
                director.run_scene (shared_ptr< Scene >(new Main_Menu));
            }
        }
    }

}
