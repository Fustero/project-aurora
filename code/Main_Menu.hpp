/*
 * END Scene
 * Copyright © 2019+ Alvaro Fustero Blasco
 *
 * alvfus@gmail.com
 */

#include <memory>
#include <basics/Scene>
#include <basics/director>
#include <basics/Texture_2D>

namespace ProjectAurora
{

    class Main_Menu : public basics::Scene
    {

        typedef std::shared_ptr< basics::Texture_2D > Texture;

    public:

        enum State
        {
            LOADING,
            RUNNING,
        };

        State          state;
        bool           suspended;

        unsigned       canvas_width;
        unsigned       canvas_height;

        Texture cursor; ///Imagen del cursor
        Texture btn_play; ///Imagen del boton de jugar
        Texture btn_exit; ///Imagen del boton de salir
        Texture btn_help; ///Imagen del boton de ayuda

        float          x, y; ///Posiciones del cursor

    public:

        Main_Menu();

        basics::Size2u get_view_size () override ///Comprobamos el tamaño de la pantalla
        {
            return { canvas_width, canvas_height };
        }

        bool initialize () override;
        void suspend    () override;
        void resume     () override;

        void handle     (basics::Event & event) override;
        void update     (float time) override;
        void render     (basics::Graphics_Context::Accessor & context) override;

    private:

        void load ();
        void run  (float time);

        void SelectMenu ();

    };

}
