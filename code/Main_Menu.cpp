/*
 * END Scene
 * Copyright © 2019+ Alvaro Fustero Blasco
 *
 * alvfus@gmail.com
 */

#include "Main_Menu.hpp"
#include "Play_Scene.hpp"
#include "Help_Scene.hpp"

#include <cstdlib>
#include <basics/Canvas>
#include <basics/Director>
#include <basics/Log>
#include <basics/Scaling>
#include <basics/Rotation>
#include <basics/Translation>

using namespace basics;
using namespace std;

namespace ProjectAurora
{

    /**
     * Asigna el tamaño del canvas a la escena
     */
    Main_Menu::Main_Menu()
    {
        canvas_width  = 1280;
        canvas_height =  720;
    }

    /**
     * Asigna el estado de cargando, de no suspendido y las pociciones iniciales del puntero
     */
    bool Main_Menu::initialize ()
    {
        state     = LOADING;
        suspended = false;
        x         = 640;
        y         = 360;

        return true;
    }

    /**
     * Suspende la escena en 2º plano
     */
    void Main_Menu::suspend ()
    {
        suspended = true;
    }

    /**
     * Se retoma la escena al volver.
     */
    void Main_Menu::resume ()
    {
        suspended = false;
    }

    /**
     * Recoge la pulsación en pantalla y si se esta ejecutando la aplicacion recoge los valores y se los asigna al puntero.
     */
    void Main_Menu::handle (Event & event)
    {
        if (state == RUNNING)
        {
            switch (event.id)
            {
                case ID(touch-started):
                case ID(touch-moved):
                case ID(touch-ended):
                {
                    x = *event[ID(x)].as< var::Float > ();
                    y = *event[ID(y)].as< var::Float > ();
                    break;
                }
            }
        }
    }

    /**
     * Dependiendo del estado se ejecuta una u otra.
     */
    void Main_Menu::update (float time)
    {
        switch (state)
        {
            case LOADING: load ();     break;
            case RUNNING: run  (time); break;
        }
    }

    void Main_Menu::render (basics::Graphics_Context::Accessor & context)
    {
        if (!suspended && state == RUNNING)
        {
            /**
             * Se crea el canvas si no existe
             */
            Canvas * canvas = context->get_renderer< Canvas > (ID(canvas));

            if (!canvas)
            {
                canvas = Canvas::create (ID(canvas), context, {{ canvas_width, canvas_height }});
            }

            if (canvas)
            {
                canvas->clear        ();
                canvas->set_color    (1, 1, 1);
                //canvas->draw_point   ({ 360, 360 });
                //canvas->draw_segment ({   0,   0 }, { 1280, 720 });
                //canvas->draw_segment ({   0, 720 }, { 1280,   0 });

                /**
                 * Se crean los botones de Jugar, Salir y Ayuda
                 */
                if  (btn_play)
                {
                    canvas->fill_rectangle ({ (canvas_width/4) , (canvas_height/4)*3 }, { 150, 150 }, btn_play.get ());
                }
                if  (btn_exit)
                {
                    canvas->fill_rectangle ({ (canvas_width/4) , (canvas_height/4) }, { 150, 150 }, btn_exit.get ());
                }
                if  (btn_help)
                {
                    canvas->fill_rectangle ({ (canvas_width/4)*3 , (canvas_height/4)*2 }, { 150, 150 }, btn_help.get ());
                }

                /**
                 * Se crea el cursor
                 */
                if (cursor)
                {
                    canvas->fill_rectangle ({ x, y }, { 100, 100 }, cursor.get ());
                }
            }
        }
    }

    void Main_Menu::load ()
    {
        if (!suspended)
        {
            Graphics_Context::Accessor context = director.lock_graphics_context ();
            /**
             * De haber contexto crean los botones y el cursor.
             */
            if (context)
            {
                btn_play = Texture_2D::create (ID(btnplay), context, "play.png");
                btn_exit = Texture_2D::create (ID(btnexit), context, "exit.png");
                btn_help = Texture_2D::create (ID(btnhelp), context, "help.png");
                cursor = Texture_2D::create (ID(cursorPly), context, "pal.png");

                if (cursor && btn_play && btn_exit && btn_help)
                {
                    context->add (cursor);
                    context->add (btn_play);
                    context->add (btn_exit);
                    context->add (btn_help);

                    state = RUNNING;
                }
            }
        }
    }

    void Main_Menu::run (float )
    {
        SelectMenu();
    }

    /**
     * Por cada frame se comprueba la posicion del cursor
     */
    void Main_Menu::SelectMenu()
    {

        if ((x < ((canvas_width/4)+50)) && (x > ((canvas_width/4)-50)))
        {
            if (y < ((canvas_height/4)*3)+50 && y > ((canvas_height/4)*3)-50) //play
            {
                director.run_scene (shared_ptr< Scene >(new Play_Scene));///De estar el cursor en play se ejecuta el juego

            }

            else if (y < (canvas_height/4)+50 && y > (canvas_height/4)-50) //exit
            {
                director.stop(); ///De estar el cursor en salir se cierra la app
            }
        }

        if ((x < ((canvas_width/4)*3)+50) && (x > ((canvas_width/4)*3)-50)) //help
        {
            if (y < ((canvas_height/4)*2)+50 && y > ((canvas_height/4)*2)-50)
            {
                director.run_scene (shared_ptr< Scene >(new Help_Scene)); ///De estar el cursor en ayuda se pasa al menu de ayuda

            }
        }
    }
}
