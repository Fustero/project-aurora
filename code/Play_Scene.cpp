/*
 * END Scene
 * Copyright © 2019+ Alvaro Fustero Blasco
 *
 * alvfus@gmail.com
 */

#include "Play_Scene.hpp"
#include "END_Scene.hpp"
#include "Main_Menu.hpp"
#include <basics/Accelerometer>
#include <basics/Canvas>
#include <basics/Director>

using namespace basics;
using namespace std;

namespace ProjectAurora
{

    /**
     * Al pasar la aplicación a 2º plano se activa la "suspension" y la "pausa".
     * A su vez se apaga el acelerometro para evitar un consumo excesivo
     */
    void Play_Scene::suspend ()
    {
        suspended = true;

        paused = true;

        Accelerometer * accelerometer = Accelerometer::get_instance ();

        if (accelerometer) accelerometer->switch_off ();
    }

    /**
     * Al volver la app al primer plano se quita la suspension y se activa el acelerometrio.
     * La pausa sigue activa.
     */
    void Play_Scene::resume ()
    {
        suspended = false;

        Accelerometer * accelerometer = Accelerometer::get_instance ();

        if (accelerometer) accelerometer->switch_on ();
    }

    void Play_Scene::update (float time)
    {
        if (!suspended){ ///En cada frame comprobamos si está "suspendido" el juego.
            load (); ///Dibuja los objetos en pantalla.

            /**
             * De no haber una fuente de letra la creamos.
             */
            if (!font)
            {
                Graphics_Context::Accessor context = director.lock_graphics_context ();

                if (context)
                {
                    font.reset (new Raster_Font("fonts/impact.fnt", context));
                }
            }
            if(!paused){

                /**
                 * De no estar el juego pausado recogemos los valores del acelerometro y se lo asignamos a la X e Y de la nave del jugador.
                 */
                Accelerometer * accelerometer = Accelerometer::get_instance ();

                if (accelerometer)
                {
                    const Accelerometer::State & acceleration = accelerometer->get_state ();

                    float roll  = atan2f ( acceleration.y, acceleration.z) * 57.3f;
                    float pitch = atan2f (-acceleration.x, sqrtf (acceleration.y * acceleration.y + acceleration.z * acceleration.z)) * 57.3f;

                    x += roll  * speed * time;
                    y += pitch * speed * time;

                    if (x - half_size <  0.f){
                        x = half_size;
                    }
                    else{}
                    if (x + half_size >= canvas_width){
                        x = canvas_width - half_size;
                    }

                    if (y - half_size <  0.f){
                        y = half_size;
                    }
                    else{}
                    if (y + half_size >= canvas_height){
                        y = canvas_height - half_size;
                    }
                }

                /**
                 * Comprobamos que los disparos no están fuera del mapa.
                 * Comprobamos que los enemigos no chocan contra una bala o contra el player.
                 */
                checkShoots(time);
                checkEnemy(time);
            }
        }

    }

    void Play_Scene::render (basics::Graphics_Context::Accessor & context)
    {
        if (!suspended)
        {

            /**
             * De no estar suspendido y de no haber camvas, lo creamos.
             */
            Canvas * canvas = context->get_renderer< Canvas > (ID(canvas));

            if (!canvas)
            {
                canvas = Canvas::create (ID(canvas), context, {{ canvas_width, canvas_height }});
            }

            if (canvas)
            {
                /**
                 * Si hay canvas lo "limpiamos" y le ponemos un color de fondo.
                 */
                canvas->clear ();
                canvas->set_color (1, 1, 1); /// Color negro simulando el espacio.

                if (!paused){///Si no está pausado...

                    if  (player)///colocamos el jugador
                    {
                        canvas->fill_rectangle ({ x-half_size , y-half_size }, { 75, 75 }, player.get ());
                    }

                    if (font)///colocamos el texto de los puntos.
                    {
                        // Se dibujan textos con diferentes puntos de anclaje a partir de una cadena simple:

                        Text_Layout sample_text(*font, to_wstring(points)); //points


                        canvas->draw_text ({ canvas_width, canvas_height }, sample_text,    TOP | RIGHT);

                    }

                    if(btn_pause){///colocamos el botón de pausa.
                        canvas->fill_rectangle ({ 50 , canvas_height-50 }, { 75, 75 }, btn_pause.get ());
                    }

                    /**
                     * El por cada bala y cada enemigo, lo colocamos en el mundo. Inicialmente fuera del mapa.
                     */
                    if  (bullet1) //Bala 1
                    {
                        canvas->fill_rectangle ({ targetBala1X , targetBala1Y }, { 50, 50 }, bullet1.get ());
                    }

                    if  (bullet2)//Bala 2
                    {
                        canvas->fill_rectangle ({ targetBala2X , targetBala2Y }, { 50, 50 }, bullet2.get ());
                    }

                    if  (bullet3)//Bala 3
                    {
                        canvas->fill_rectangle ({ targetBala3X , targetBala3Y }, { 50, 50 }, bullet3.get ());
                    }
                    if  (bullet4)//Bala 4
                    {
                        canvas->fill_rectangle ({ targetBala4X , targetBala4Y }, { 50, 50 }, bullet4.get ());
                    }
                    if  (bullet5)//Bala 5
                    {
                        canvas->fill_rectangle ({ targetBala5X , targetBala5Y }, { 50, 50 }, bullet5.get ());
                    }
                    if  (bullet6)//Bala 6
                    {
                        canvas->fill_rectangle ({ targetBala6X , targetBala6Y }, { 50, 50 }, bullet6.get ());
                    }

                    if  (enemy1) //Enemigo 1
                    {
                        canvas->fill_rectangle ({ targetEnemy1X , targetEnemy1Y }, { 75, 54 }, enemy1.get ());
                    }
                    if  (enemy2)//Enemigo 2
                    {
                        canvas->fill_rectangle ({ targetEnemy2X , targetEnemy2Y }, { 75, 54 }, enemy2.get ());
                    }
                    if  (enemy3)//Enemigo 3
                    {
                        canvas->fill_rectangle ({ targetEnemy3X , targetEnemy3Y }, { 75, 54 }, enemy3.get ());
                    }
                    if  (enemy4)//Enemigo 4
                    {
                        canvas->fill_rectangle ({ targetEnemy4X , targetEnemy4Y }, { 75, 54 }, enemy4.get ());
                    }
                }
                if(paused){

                    /**
                     * De estar el juego pausado creamos el texto de pausa
                     * y los botones de jugar o ir al menú.
                     */
                    if (font)
                    {


                        Text_Layout sample_text(*font, L"Juego Pausado"); //points


                        canvas->draw_text ({ canvas_width/2, canvas_height/2 }, sample_text,    CENTER);


                    }

                    if(btn_play){
                        canvas->fill_rectangle ({ (canvas_width/4) , (canvas_height/4) }, { 100, 100 }, btn_play.get ());
                    }
                    if(btn_menu){
                        canvas->fill_rectangle ({ (canvas_width/4)*3 , (canvas_height/4) }, { 100, 100 }, btn_menu.get ());
                    }
                }
            }
        }
    }

    void Play_Scene::load ()
    {
        if (!suspended)
        {

            Graphics_Context::Accessor context = director.lock_graphics_context ();///Creamos el "contexto".
            if (context)
            {
                /**
                 * De no estar el juego pausado renllenamos los enemigos, balas y los botones/nave.
                 */
                if(!paused)
                {

                    btn_pause = Texture_2D::create (ID(pauseID), context, "pausa.png");

                    if (btn_pause)
                    {
                        context->add (btn_pause);
                    }

                    player = Texture_2D::create (ID(playerID), context, "ship1.png");

                    if (player)
                    {
                        context->add (player);
                    }

                    bullet1 = Texture_2D::create (ID(bullet1ID), context, "bullet.png"); //bala 1
                    bullet2 = Texture_2D::create (ID(bullet2ID), context, "bullet.png"); //bala 2
                    bullet3 = Texture_2D::create (ID(bullet3ID), context, "bullet.png"); //bala 3
                    bullet4 = Texture_2D::create (ID(bullet4ID), context, "bullet.png"); //bala 4
                    bullet5 = Texture_2D::create (ID(bullet5ID), context, "bullet.png"); //bala 5
                    bullet6 = Texture_2D::create (ID(bullet6ID), context, "bullet.png"); //bala 6

                    if(bullet1){
                        context->add (bullet1);
                    }
                    if(bullet2){
                        context->add (bullet2);
                    }
                    if(bullet3){
                        context->add (bullet3);
                    }
                    if(bullet4){
                        context->add (bullet4);
                    }
                    if(bullet5){
                        context->add (bullet5);
                    }
                    if(bullet6){
                        context->add (bullet6);
                    }

                    enemy1 = Texture_2D::create (ID(enemy1ID), context, "invader1.png"); //Enemigo 1
                    enemy2 = Texture_2D::create (ID(enemy2ID), context, "invader1.png"); //Enemigo 2
                    enemy3 = Texture_2D::create (ID(enemy3ID), context, "invader1.png"); //Enemigo 3
                    enemy4 = Texture_2D::create (ID(enemy4ID), context, "invader1.png"); //Enemigo 4

                    if(enemy1){
                        context->add (enemy1);
                    }if(enemy2){
                        context->add (enemy2);
                    }if(enemy3){
                        context->add (enemy3);
                    }if(enemy4){
                        context->add (enemy4);
                    }
                }
                if(paused){
                    /**
                     * De estar el juego pausado rellenamos los botones del menú de pausa.
                     */
                    btn_menu = Texture_2D::create (ID(btn_menuID), context, "menu.png");

                    if (btn_menu)
                    {
                        context->add (btn_menu);
                    }

                    btn_play = Texture_2D::create (ID(btn_playID), context, "play.png");

                    if (btn_play)
                    {
                        context->add (btn_play);
                    }
                }
            }
        }
    }

    void Play_Scene::handle (Event & event)
    {
        if (!suspended)
        {
            /**
             * Por cada toque cuando el juego está en primer plano
             * recogemos los valores de X e Y, con un delay para no hacer un punteo abusivo.
             */
            switch (event.id)
            {
                case ID(touch-ended):
                    break;///Evitamos que el levantar el móvil lo detecte como un toque.
                case ID(touch-moved):
                case ID(touch-started):
                {
                    /**
                     * De no estar el juego pausado recogemos los valores de X e Y.
                     * Si están dentro del recuadro de pausa, ejecutamos la pausa
                     *
                     * De estar fuera ejecutamos el disparo.
                     */
                    if (!paused){
                        if (timer.get_elapsed_seconds() > 0.5f)
                        {

                            targetX = *event[ID(x)].as< var::Float > ();
                            targetY = *event[ID(y)].as< var::Float > ();
                            if((targetX >= 50-50) && (targetX <= 50+50)){
                                if(targetY >= canvas_height-50 && targetY <= canvas_height+50){

                                    PauseGame();
                                    timer.reset(); ///reinicia el contador del delay
                                    break;///Evita que continue el proceso
                                }
                            }
                            shoot();

                            timer.reset();///reinicia el contador del delay
                            break;
                        }
                    }
                    if (paused){
                        targetX = *event[ID(x)].as< var::Float > ();
                        targetY = *event[ID(y)].as< var::Float > ();

                        /***
                         * De estar el toque en el boton de play quitamos la pausa y de estar en el del menú volvemos a este.
                         */
                        if((targetX < ((canvas_width/4)+100)) && (targetX > ((canvas_width/4)-100))){
                            if(targetY < (canvas_height/4)+100 && targetY > (canvas_height/4)-100){


                                NOPauseGame();
                                timer.reset();
                                break;
                            }
                        }

                        if((targetX < ((canvas_width/4)*3+100)) && (targetX > ((canvas_width/4)*3-100))){
                            if(targetY < (canvas_height/4)+100 && targetY > (canvas_height/4)-100){

                                director.run_scene (shared_ptr< Scene >(new Main_Menu));///Vuelve al menú

                                timer.reset();
                                break;
                            }
                        }
                    }
                    break;
                }
            }
        }
    }

    void  Play_Scene::shoot(){

        /**
         * Este Código se repite por cada disparo comprobando si ha sido disparado. De estar en vuelo pasa a la siguiente bala para no modificar la trayecytoria del anterior.
         */
        if(!bullet1Shooted) //bala 1
        {
            bullet1Shooted = true; ///Evita la modificación de trayectoria.

            targetBala1X = x;
            targetBala1Y = y;

            float angulo = atan2(targetY - targetBala1Y, targetX - targetBala1X); ///Calcula el ángulo de la distancia entre la nave y donde se ha disparado.

            vectorBala1X = cos(angulo); ///Asignamos las "distancias".
            vectorBala1Y = sin(angulo); ///Asignamos las "distancias".

        }
        else if(!bullet2Shooted) //bala 2
        {
            bullet2Shooted = true;

            targetBala2X = x;
            targetBala2Y = y;

            float angulo = atan2(targetY - targetBala2Y, targetX - targetBala2X);

            vectorBala2X = cos(angulo);
            vectorBala2Y = sin(angulo);
        }
        else if(!bullet3Shooted) //bala 3
        {
            bullet3Shooted = true;

            targetBala3X = x;
            targetBala3Y = y;

            float angulo = atan2(targetY - targetBala3Y, targetX - targetBala3X);

            vectorBala3X = cos(angulo);
            vectorBala3Y = sin(angulo);
        }
        else if(!bullet4Shooted) //bala 4
        {
            bullet4Shooted = true;

            targetBala4X = x;
            targetBala4Y = y;

            float angulo = atan2(targetY - targetBala4Y, targetX - targetBala4X);

            vectorBala4X = cos(angulo);
            vectorBala4Y = sin(angulo);
        }
        else if(!bullet5Shooted) //bala 5
        {
            bullet5Shooted = true;

            targetBala5X = x;
            targetBala5Y = y;

            float angulo = atan2(targetY - targetBala5Y, targetX - targetBala5X);

            vectorBala5X = cos(angulo);
            vectorBala5Y = sin(angulo);
        }
        else if(!bullet6Shooted) //bala 6
        {
            bullet6Shooted = true;

            targetBala6X = x;
            targetBala6Y = y;

            float angulo = atan2(targetY - targetBala6Y, targetX - targetBala6X);

            vectorBala6X = cos(angulo);
            vectorBala6Y = sin(angulo);
        }

    }

    void Play_Scene::checkShoots(float time){
        /**
         * Este Código se repite por cada disparo comprobando si ha sido disparado y calculando la ruta del proyectil.
         * Como en la función anterior se comprueba si está en vuelo para evitar su modificación, estos valores son fijos..
         */
        if(bullet1Shooted) //bala 1
        {
            /**
             * De haber sido disparado asignamos la posición en cada frame multiplicándolo por time para evitar la diferencia de frames.
             */
            targetBala1X = targetBala1X + vectorBala1X * 200 * time;

            targetBala1Y = targetBala1Y + vectorBala1Y * 200 * time;

            /**
             * Si la bala sale del escenario se coloca estática fuera de este y se indica que no ha sido disparada.
             */
            if(targetBala1X > canvas_width || targetBala1X < 0 || targetBala1Y > canvas_width || targetBala1Y < 0){
                bullet1Shooted = false;
                vectorBala1X = 2000;
                vectorBala1Y = 2000;
                targetBala1X = 2000;
                targetBala1Y = 2000;
            }
        }
       /* else{///Por si hay algún problema, si la bala no ha sido disparada que deja estática".
            vectorBala1X = 2000;
            vectorBala1Y = 2000;
        }*/

        if(bullet2Shooted) //bala 2
        {
            targetBala2X = targetBala2X + vectorBala2X * 200 * time;

            targetBala2Y = targetBala2Y + vectorBala2Y * 200 * time;

            if(targetBala2X > canvas_width || targetBala2X < 0.f || targetBala2Y > canvas_width || targetBala2Y < 0){
                bullet2Shooted = false;
                vectorBala2X = 2000;
                vectorBala2Y = 2000;
                targetBala2X = 2000;
                targetBala2Y = 2000;
            }
        }
        /*else{
            vectorBala2X = 2000;
            vectorBala2Y = 2000;
        }*/

        if(bullet3Shooted) //bala 3
        {
            targetBala3X = targetBala3X + vectorBala3X * 200 * time;

            targetBala3Y = targetBala3Y + vectorBala3Y * 200 * time;

            if(targetBala3X > canvas_width || targetBala3X < 0 || targetBala3Y > canvas_width || targetBala3Y < 0){
                bullet3Shooted = false;
                vectorBala3X = 2000;
                vectorBala3Y = 2000;
                targetBala3X = 2000;
                targetBala3Y = 2000;
            }
        }
        /*else{
            vectorBala3X = 2000;
            vectorBala3Y = 2000;
        }*/
        if(bullet4Shooted) //bala 4
        {
            targetBala4X = targetBala4X + vectorBala4X * 200 * time;

            targetBala4Y = targetBala4Y + vectorBala4Y * 200 * time;

            if(targetBala4X > canvas_width || targetBala4X < 0 || targetBala4Y > canvas_width || targetBala4Y < 0){
                bullet4Shooted = false;
                vectorBala4X = 2000;
                vectorBala4Y = 2000;
                targetBala4X = 2000;
                targetBala4Y = 2000;
            }
        }
        /*else{
            vectorBala4X = 2000;
            vectorBala4Y = 2000;
        }*/
        if(bullet5Shooted) //bala 5
        {
            targetBala5X = targetBala5X + vectorBala5X * 200 * time;

            targetBala5Y = targetBala5Y + vectorBala5Y * 200 * time;

            if(targetBala5X > canvas_width || targetBala5X < 0 || targetBala5Y > canvas_width || targetBala5Y < 0){
                bullet5Shooted = false;
                vectorBala5X = 2000;
                vectorBala5Y = 2000;
                targetBala5X = 2000;
                targetBala5Y = 2000;
            }
        }
        /*else{
            vectorBala5X = 2000;
            vectorBala5Y = 2000;
        }*/
        if(bullet6Shooted) //bala 6
        {
            targetBala6X = targetBala6X + vectorBala6X * 200 * time;

            targetBala6Y = targetBala6Y + vectorBala6Y * 200 * time;

            if(targetBala6X > canvas_width || targetBala6X < 0 || targetBala6Y > canvas_width || targetBala6Y < 0){
                bullet6Shooted = false;
                vectorBala6X = 2000;
                vectorBala6Y = 2000;
                targetBala6X = 2000;
                targetBala6Y = 2000;
            }
        }
        /*else{
            vectorBala6X = 2000;
            vectorBala6Y = 2000;
        }*/
    }

    void Play_Scene::checkEnemy(float time) {

        /**
         * Este código se repite por cada enemigo asignando su ruta hacia la nave y comprobando si ha impactado con una bala o la nave.
         */
        //Enemigo 1
        float angulo1 = atan2(x - targetEnemy1X, y - targetEnemy1Y); ///Sacamos el ángulo entre la nave y el proyectil.

        vectorEnemy1X = sin(angulo1);///Sacamos la distancia lateral y vertical.
        vectorEnemy1Y = cos(angulo1);

        targetEnemy1X = targetEnemy1X + vectorEnemy1X * 50 * time; ///Sumamos la distancia multiplicándola por time para evitar la diferencia de frames.
        targetEnemy1Y = targetEnemy1Y + vectorEnemy1Y * 50 * time;

        //Enemigo 2
        float angulo2 = atan2(x - targetEnemy2X, y - targetEnemy2Y);

        vectorEnemy2X = sin(angulo2);
        vectorEnemy2Y = cos(angulo2);

        targetEnemy2X = targetEnemy2X + vectorEnemy2X * 50 * time;
        targetEnemy2Y = targetEnemy2Y + vectorEnemy2Y * 50 * time;

        //Enemigo 3
        float angulo3 = atan2(x - targetEnemy3X, y - targetEnemy3Y);

        vectorEnemy3X = sin(angulo3);
        vectorEnemy3Y = cos(angulo3);

        targetEnemy3X = targetEnemy3X + vectorEnemy3X * 50 * time;
        targetEnemy3Y = targetEnemy3Y + vectorEnemy3Y * 50 * time;

        //Enemigo 4
        float angulo4 = atan2(x - targetEnemy4X, y - targetEnemy4Y);

        vectorEnemy4X = sin(angulo4);
        vectorEnemy4Y = cos(angulo4);

        targetEnemy4X = targetEnemy4X + vectorEnemy4X * 50 * time;
        targetEnemy4Y = targetEnemy4Y + vectorEnemy4Y * 50 * time;


        /**
         * Si el enemigo impacta con una bala este se coloca fuera del mapa para que vuelva a entrar y se suma 1 punto.
         */
        //Enemigo 1
        if(targetEnemy1X > targetBala1X -50 && targetEnemy1X < targetBala1X + 50){ //bala 1
            if(targetEnemy1Y > targetBala1Y -50 && targetEnemy1Y < targetBala1X + 50){
                targetEnemy1Y = -100;
                targetEnemy1X = -100;

                points++;
            }
        }
        else if(targetEnemy1X > targetBala2X -50 && targetEnemy1X < targetBala2X + 50){ //bala2
            if(targetEnemy1Y > targetBala2Y -50 && targetEnemy1Y < targetBala2X + 50){
                targetEnemy1Y = -100;
                targetEnemy1X = -100;

                points++;
            }
        }
        else if(targetEnemy1X > targetBala3X -50 && targetEnemy1X < targetBala3X + 50){ //bala 3
            if(targetEnemy1Y > targetBala3Y -50 && targetEnemy1Y < targetBala3X + 50){
                targetEnemy1Y = -100;
                targetEnemy1X = -100;

                points++;
            }
        }
        else if(targetEnemy1X > targetBala4X -50 && targetEnemy1X < targetBala4X + 50){ //bala 4
            if(targetEnemy1Y > targetBala4Y -50 && targetEnemy1Y < targetBala4X + 50){
                targetEnemy1Y = -100;
                targetEnemy1X = -100;

                points++;
            }
        }
        else if(targetEnemy1X > targetBala5X -50 && targetEnemy1X < targetBala5X + 50){ //bala 5
            if(targetEnemy1Y > targetBala5Y -50 && targetEnemy1Y < targetBala5X + 50){
                targetEnemy1Y = -100;
                targetEnemy1X = -100;

                points++;
            }
        }
        else if(targetEnemy1X > targetBala6X -50 && targetEnemy1X < targetBala6X + 50){ //bala 6
            if(targetEnemy1Y > targetBala6Y -50 && targetEnemy1Y < targetBala6X + 50){
                targetEnemy1Y = -100;
                targetEnemy1X = -100;

                points++;
            }
        }

        //Enemigo 2
        if(targetEnemy2X > targetBala1X -50 && targetEnemy2X < targetBala1X + 50){ //bala 1
            if(targetEnemy2Y > targetBala1Y -50 && targetEnemy2Y < targetBala1X + 50){
                targetEnemy2Y = canvas_height+100;
                targetEnemy2X = -100;

                points++;
            }
        }
        else if(targetEnemy2X > targetBala2X -50 && targetEnemy2X < targetBala2X + 50){ //bala 2
            if(targetEnemy2Y > targetBala2Y -50 && targetEnemy2Y < targetBala2X + 50){
                targetEnemy2Y = canvas_height+100;
                targetEnemy2X = -100;

                points++;
            }
        }
        else if(targetEnemy2X > targetBala3X -50 && targetEnemy2X < targetBala3X + 50){ //bala 3
            if(targetEnemy2Y > targetBala3Y -50 && targetEnemy2Y < targetBala3X + 50){
                targetEnemy2Y = canvas_height+100;
                targetEnemy2X = -100;

                points++;
            }
        }
        else if(targetEnemy2X > targetBala4X -50 && targetEnemy2X < targetBala4X + 50){ //bala 4
            if(targetEnemy2Y > targetBala4Y -50 && targetEnemy2Y < targetBala4X + 50){
                targetEnemy2Y = canvas_height+100;
                targetEnemy2X = -100;

                points++;
            }
        }
        else if(targetEnemy2X > targetBala5X -50 && targetEnemy2X < targetBala5X + 50){ //bala 5
            if(targetEnemy2Y > targetBala5Y -50 && targetEnemy2Y < targetBala5X + 50){
                targetEnemy2Y = canvas_height+100;
                targetEnemy2X = -100;

                points++;
            }
        }
        else if(targetEnemy2X > targetBala6X -50 && targetEnemy2X < targetBala6X + 50){ //bala 6
            if(targetEnemy2Y > targetBala6Y -50 && targetEnemy2Y < targetBala6X + 50){
                targetEnemy2Y = canvas_height+100;
                targetEnemy2X = -100;

                points++;
            }
        }

        //Enemigo 3
        if(targetEnemy3X > targetBala1X -50 && targetEnemy3X < targetBala1X + 50){ //bala 1
            if(targetEnemy3Y > targetBala1Y -50 && targetEnemy3Y < targetBala1X + 50){
                targetEnemy3Y = -100;
                targetEnemy3X = canvas_width+100;

                points++;
            }
        }
        else if(targetEnemy3X > targetBala2X -50 && targetEnemy3X < targetBala2X + 50){ //bala 2
            if(targetEnemy3Y > targetBala2Y -50 && targetEnemy3Y < targetBala2X + 50){
                targetEnemy3Y = -100;
                targetEnemy3X = canvas_width+100;

                points++;
            }
        }
        else if(targetEnemy3X > targetBala3X -50 && targetEnemy3X < targetBala3X + 50){ //bala 3
            if(targetEnemy3Y > targetBala3Y -50 && targetEnemy3Y < targetBala3X + 50){
                targetEnemy3Y = -100;
                targetEnemy3X = canvas_width+100;

                points++;
            }
        }
        else if(targetEnemy3X > targetBala4X -50 && targetEnemy3X < targetBala4X + 50){ //bala 4
            if(targetEnemy3Y > targetBala4Y -50 && targetEnemy3Y < targetBala4X + 50){
                targetEnemy3Y = -100;
                targetEnemy3X = canvas_width+100;

                points++;
            }
        }
        else if(targetEnemy3X > targetBala5X -50 && targetEnemy3X < targetBala5X + 50){ //bala 5
            if(targetEnemy3Y > targetBala5Y -50 && targetEnemy3Y < targetBala5X + 50){
                targetEnemy3Y = -100;
                targetEnemy3X = canvas_width+100;

                points++;
            }
        }
        else if(targetEnemy3X > targetBala6X -50 && targetEnemy3X < targetBala6X + 50){ //bala 6
            if(targetEnemy3Y > targetBala6Y -50 && targetEnemy3Y < targetBala6X + 50){
                targetEnemy3Y = -100;
                targetEnemy3X = canvas_width+100;

                points++;
            }
        }


        //Enemigo 4
        if(targetEnemy4X > targetBala1X -50 && targetEnemy4X < targetBala1X + 50){ //bala1
            if(targetEnemy4Y > targetBala1Y -50 && targetEnemy4Y < targetBala1X + 50){
                targetEnemy4Y = canvas_height+100;
                targetEnemy4X = -1380;

                points++;
            }
        }
        else if(targetEnemy4X > targetBala2X -50 && targetEnemy4X < targetBala2X + 50){ //bala 2
            if(targetEnemy4Y > targetBala2Y -50 && targetEnemy4Y < targetBala2X + 50){
                targetEnemy4Y = canvas_height+100;
                targetEnemy4X = canvas_width+100;

                points++;
            }
        }
        else if(targetEnemy4X > targetBala3X -50 && targetEnemy4X < targetBala3X + 50){ //bala 3
            if(targetEnemy4Y > targetBala3Y -50 && targetEnemy4Y < targetBala3X + 50){
                targetEnemy4Y = canvas_height+100;
                targetEnemy4X = canvas_width+100;

                points++;
            }
        }
        else if(targetEnemy4X > targetBala4X -50 && targetEnemy4X < targetBala4X + 50){ //bala 4
            if(targetEnemy4Y > targetBala4Y -50 && targetEnemy4Y < targetBala4X + 50){
                targetEnemy4Y = canvas_height+100;
                targetEnemy4X = canvas_width+100;

                points++;
            }
        }
        else if(targetEnemy4X > targetBala5X -50 && targetEnemy4X < targetBala5X + 50){ //bala 5
            if(targetEnemy4Y > targetBala5Y -50 && targetEnemy4Y < targetBala5X + 50){
                targetEnemy4Y = canvas_height+100;
                targetEnemy4X = canvas_width+100;

                points++;
            }
        }
        else if(targetEnemy3X > targetBala6X -50 && targetEnemy3X < targetBala6X + 50){//bala 6
            if(targetEnemy4Y > targetBala6Y -50 && targetEnemy4Y < targetBala6X + 50){
                targetEnemy4Y = canvas_height+100;
                targetEnemy4X = canvas_width+100;

                points++;
            }
        }

        /**
         * Si un enemgio impacta a la nave se pasa al menu de derrota.
         */

        if((x > targetEnemy1X - 50 && x < targetEnemy1X + 50 && y > targetEnemy1Y - 50 && y < targetEnemy1Y + 50)||(x > targetEnemy2X - 50 && x < targetEnemy2X + 50 && y > targetEnemy2Y - 50 && y < targetEnemy2Y + 50)||(x > targetEnemy3X - 50 && x < targetEnemy3X + 50 && y > targetEnemy3Y - 50 && y < targetEnemy3Y + 50)||(x > targetEnemy4X - 50 && x < targetEnemy4X + 50 && y > targetEnemy4Y - 50 && y < targetEnemy4Y + 50)){
            //puntos = points;
            director.run_scene (shared_ptr< Scene >(new END_Scene));
        }


    }

    void Play_Scene::PauseGame(){

        paused = true;///Pausa el juego

    }

    void Play_Scene::NOPauseGame(){
        paused = false; ///Quita la pausa
    }
}
