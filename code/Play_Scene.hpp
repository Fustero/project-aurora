/*
 * END Scene
 * Copyright © 2019+ Alvaro Fustero Blasco
 *
 * alvfus@gmail.com
 */

#include <memory>
#include <basics/Canvas>
#include <basics/Scene>
#include <basics/Timer>


namespace ProjectAurora
{

    using basics::Size2f;
    using basics::Point2f;
    using basics::Vector2f;
    using basics::Timer;

    class Play_Scene : public basics::Scene
    {
        typedef std::shared_ptr< basics::Texture_2D > Texture;
        typedef std::unique_ptr< basics::Raster_Font > Font_Handle;
    private:

        bool     suspended;                         ///< true cuando la aplicación está en segundo plano
        bool     paused;                         ///< true cuando la aplicación está en segundo plano
        unsigned canvas_width;                      ///< Resolución virtual del display
        unsigned canvas_height;
        float    half_size;

        float    x; ///posicion X de la nave
        float    y; ///Posicion Y de la nave
        float    speed;  ///Velocidad


        float targetX; ///Pulsación X
        float targetY; ///Pulsacion Y


        void load ();
        Font_Handle font; ///fuente de letra
        Texture player; ///textura nave

        /**
         * Texturas enemigos
         */
        Texture enemy1;
        Texture enemy2;
        Texture enemy3;
        Texture enemy4;

        /**
         * valores del objetivo de los enemgios
         */
        float vectorEnemy1X; ///X
        float vectorEnemy2X;
        float vectorEnemy3X;
        float vectorEnemy4X;

        float vectorEnemy1Y; ///Y
        float vectorEnemy2Y;
        float vectorEnemy3Y;
        float vectorEnemy4Y;

        /**
         * Valores de inicio y posición en cada frame de los enemigos
         */
        float targetEnemy1X; ///X
        float targetEnemy2X;
        float targetEnemy3X;
        float targetEnemy4X;

        float targetEnemy1Y; ///Y
        float targetEnemy2Y;
        float targetEnemy3Y;
        float targetEnemy4Y;


        Timer timer; ///Contador

        Texture btn_play; ///Textura del boton de jugar del menu de pausa
        Texture btn_menu; ///Textura del boton de volver al menu en el menu de pausa
        Texture btn_pause; ///Textura del boton de pausa

        /**
         * Texturas de las balas
         */
        Texture bullet1;
        Texture bullet2;
        Texture bullet3;
        Texture bullet4;
        Texture bullet5;
        Texture bullet6;

        /**
         * Valores que comprueban si las balas han sido disparadas.
         */
        bool bullet1Shooted;
        bool bullet2Shooted;
        bool bullet3Shooted;
        bool bullet4Shooted;
        bool bullet5Shooted;
        bool bullet6Shooted;

        /**
         * Valores de los "objetivos" de las balas
         */
        float vectorBala1X; ///X
        float vectorBala2X;
        float vectorBala3X;
        float vectorBala4X;
        float vectorBala5X;
        float vectorBala6X;

        float vectorBala1Y; ///Y
        float vectorBala2Y;
        float vectorBala3Y;
        float vectorBala4Y;
        float vectorBala5Y;
        float vectorBala6Y;

        /**
         * Valores de la posicion de las balas
         */
        float targetBala1X; ///X
        float targetBala2X;
        float targetBala3X;
        float targetBala4X;
        float targetBala5X;
        float targetBala6X;

        float targetBala1Y; ///Y
        float targetBala2Y;
        float targetBala3Y;
        float targetBala4Y;
        float targetBala5Y;
        float targetBala6Y;

        int points   ; ///Puntuacion

    public:



        Play_Scene()
        {
            canvas_width  = 1280;                   ///tamaño del canvas
            canvas_height =  720;                   ///tamaño del canvas
            half_size     =   25;
            speed         =   10;



        }

        basics::Size2u get_view_size () override
        {
            return { canvas_width, canvas_height };///Coge el aspectratio de la pantalla par ael canvas.
        }

        bool initialize () override///Al iniciar...
        {
            x = canvas_width  / 2.f; ///Posicion inicial nave (centro)
            y = canvas_height / 2.f; ///Posicion inicial nave (centro)

            targetX = 640; ///Posicion inicial del puntero
            targetY = 360; ///Posicion incial puntero

            timer.reset(); ///Resetea el timer al empezar

            suspended = false; ///Quita la suspension
            paused = false; ///quita la pausa


            points = 0; ///Puntos iniciales


            /**
             * Posicion objetivo inicial de las balas. Impide que se muevan.
             */
            vectorBala1X = 2000; ///X
            vectorBala2X = 2000;
            vectorBala3X = 2000;
            vectorBala4X = 2000;
            vectorBala5X = 2000;
            vectorBala6X = 2000;

            vectorBala1Y = 2000; ///Y
            vectorBala2Y = 2000;
            vectorBala3Y = 2000;
            vectorBala4Y = 2000;
            vectorBala5Y = 2000;
            vectorBala6Y = 2000;

            /**
             * Posicion Inicial balas
             */
            targetBala1X = 2000; ///X
            targetBala2X = 2000;
            targetBala3X = 2000;
            targetBala4X = 2000;
            targetBala5X = 2000;
            targetBala6X = 2000;

            targetBala1Y = 2000; ///Y
            targetBala2Y = 2000;
            targetBala3Y = 2000;
            targetBala4Y = 2000;
            targetBala5Y = 2000;
            targetBala6Y = 2000;

            /**
             * Las balas no han sido disparadas
             */
            bullet1Shooted = false;
            bullet2Shooted = false;
            bullet3Shooted = false;
            bullet4Shooted = false;
            bullet5Shooted = false;
            bullet6Shooted = false;

            /**
             * Objetivo inicial de los nemeigos.
             */
            vectorEnemy1X = 0; ///X
            vectorEnemy2X = 0;
            vectorEnemy3X = 0;
            vectorEnemy4X = 0;

            vectorEnemy1Y = 0; ///Y
            vectorEnemy2Y = 0;
            vectorEnemy3Y = 0;
            vectorEnemy4Y = 0;

            /**
             * Posicion inicial de los objetivos fuera de la pantalla
             */
            targetEnemy1X = -100; ///X
            targetEnemy2X = -100;
            targetEnemy3X = canvas_width+100;
            targetEnemy4X = canvas_width+100;

            targetEnemy1Y = -100; ///Y
            targetEnemy2Y = canvas_height+100;
            targetEnemy3Y = -100;
            targetEnemy4Y = canvas_height+100;

            return true;
        }




        void suspend () override;
        void resume () override;

        void handle     (basics::Event & event) override;
        void update (float ) override;
        void render (basics::Graphics_Context::Accessor & context) override;
        void shoot();
        void checkShoots(float );
        void checkEnemy(float );
        void PauseGame();
        void NOPauseGame();

    };

}
